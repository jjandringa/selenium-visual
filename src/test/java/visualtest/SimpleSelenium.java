package visualtest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.applitools.eyes.Eyes;
import com.applitools.eyes.MatchLevel;
import com.applitools.eyes.RectangleSize;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 *
 * @author jjand
 */
public class SimpleSelenium {
    
    // https://eyes.applitools.com/app/test-results

    public static void main(String[] args) {

        System.setProperty("webdriver.gecko.driver", "geckodriver");
        WebDriver driver = new FirefoxDriver();

        Eyes eyes = new Eyes();
        eyes.setApiKey("");
        eyes.setMatchLevel(MatchLevel.LAYOUT2);

        try {
            
            driver = eyes.open(driver, "PluralSight.com", "Selenium Visual", new RectangleSize(1200, 1200));
            driver.get("https://www.pluralsight.com/");

            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("webpush-onsite")));
            driver.switchTo().frame(driver.findElement(By.name("webpush-onsite")));
            driver.findElement(By.id("deny")).click();
            driver.switchTo().defaultContent();
            driver.findElement(By.linkText("ACCEPT COOKIES AND CLOSE THIS MESSAGE")).click();

            eyes.checkWindow("Pluralsight Main Page");
            
            driver.findElement(By.linkText("Courses")).click();
            
            JavascriptExecutor js = (JavascriptExecutor) driver;
            js.executeScript("var title = document.getElementsByClassName('first')[0]; " +
                    "title.style.position = 'relative'; " +
                    "title.style.left = '50px';");                   
            eyes.checkWindow("Browse Courses");
            
            eyes.close();

        } finally {

            driver.quit();
        }

    }
}
